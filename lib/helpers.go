package lib

import (
	"fmt"
	"html"
)

func (d *TgHTML) GetCurrentData() string{
	return d.data
}

func (d *TgHTML) SetData(str string) {
	d.data = str
}

func ToBold(str string) string {
	value := BoldOpening + html.EscapeString(str) + BoldClosing
	return value
}

func ToCode(str string) string {
	value := MonoOpening + html.EscapeString(str) + MonoClosing
	return value
}

func ToItalic(str string) string {
	value := ItalicOpening + html.EscapeString(str) + ItalicClosing
	return value
}

func GetLinkMarkup(name string, link string) string {
	value := fmt.Sprintf("<a href=%s>%s</a>", link, name)
	return value
}

func ToTgHTML(str string) fmtTG {
	if len(str) == 0 {
		return nil
	}
	return &TgHTML{data: str}
}