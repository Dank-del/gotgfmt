package lib

type TgHTML struct {
	data string
}

type fmtTG interface {
	AppendNewLine() fmtTG
    AppendBold(str string) fmtTG
	AppendItalic(str string) fmtTG
	AppendMono(str string) fmtTG

}

func (d *TgHTML) AppendNewLine() fmtTG {
	data := d.GetCurrentData() + "\n"
	d.SetData(data)
	return ToTgHTML(d.GetCurrentData())
}

func (d *TgHTML) AppendBold(str string) fmtTG {
	data := d.GetCurrentData() + ToBold(str)
	d.SetData(data)
	return ToTgHTML(d.GetCurrentData())
}

func (d *TgHTML) AppendItalic(str string) fmtTG {
	data := d.GetCurrentData() + ToItalic(str)
	d.SetData(data)
	return ToTgHTML(d.GetCurrentData())
}

func (d *TgHTML) AppendMono(str string) fmtTG {
	data := d.GetCurrentData() + ToCode(str)
	d.SetData(data)
	return ToTgHTML(d.GetCurrentData())
}


