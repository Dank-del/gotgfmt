package lib

const (
	BoldOpening  = "<b>"
	BoldClosing  = "</b>"

	ItalicOpening = "<i>"
	ItalicClosing = "</i>"

	MonoOpening   = "<code>"
	MonoClosing   = "</code>"


)
