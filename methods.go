package gotgfmt

import "gitlab.com/Dank-del/gotgfmt/lib"

func GetNormal(data string) *lib.TgHTML {
    v := new(lib.TgHTML)
    v.SetData(data)
    return v
}

func GetBold(data string) *lib.TgHTML {
    v := new(lib.TgHTML)
    v.SetData(lib.ToBold(data))
    return v
}

func GetItalic(data string) *lib.TgHTML {
    v := new(lib.TgHTML)
    v.SetData(lib.ToItalic(data))
    return v
}

func GetMono(data string) *lib.TgHTML {
    v := new(lib.TgHTML)
    v.SetData(lib.ToCode(data))
    return v
}
